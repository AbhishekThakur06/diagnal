package app.example.diagnal.myapplication;

import android.app.Application;
import android.content.Context;

/*import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;*/
import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;


@ReportsCrashes(
        formKey = "", // This is required for backward compatibility but not used
        // formUri = "https://docs.google.com/spreadsheets/d/183YLFirmJ4HHkwk_1Bp-mGVddQf4_HJ0XIvZ8t_ques/edit#gid=0",
        mailTo = "abhi.nic06@gmail.com",
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.crash_string

)
public class MyApplication extends Application {
    private static MyApplication sMyApplication;

    @Override
    public void onCreate() {
        // The following line triggers the initialization of ACRA
        super.onCreate();
        sMyApplication = this;
        ACRA.init(this);

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
       // MultiDex.install(this);
    }

    public static synchronized MyApplication getInstance() {
        return sMyApplication;
    }


}