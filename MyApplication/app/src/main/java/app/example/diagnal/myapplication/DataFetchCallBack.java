package app.example.diagnal.myapplication;

import example.Page;

/**
 * Created by Abhishek on 12/19/2016.
 */

public interface DataFetchCallBack {
    public void onDataFetched(Page page);
}
