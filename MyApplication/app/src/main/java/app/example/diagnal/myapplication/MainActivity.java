package app.example.diagnal.myapplication;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.w3c.dom.Text;

import java.io.InputStream;
import java.util.ArrayList;

import example.Content;
import example.Example;
import example.Page;
import widget.MyScrollListener;

public class MainActivity extends AppCompatActivity implements DataFetchCallBack {
    private Gson gson = new Gson();
    private int totalItems = 0;
    private int totalFetched = 0;
    private GridView gridView;
    public ArrayList<Content> mContentList;
    private ImageAdapter imageAdapter;
    private int pageNumber = 0;
    private TextView title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fr_image_grid);
        gridView = (GridView) findViewById(R.id.grid);
        readPageAndUpdate(getResourceId(pageNumber),this);
        ActionBar actionBar = getSupportActionBar();
        //actionBar.setBackgroundDrawable(getResources().getColor(R.color.theme_color));
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#000000")));
        actionBar.setCustomView(R.layout.actionbar);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.getCustomView().findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        title = (TextView) actionBar.getCustomView().findViewById(R.id.title);
    }

    @Override
    public void onDataFetched(Page page) {
        if(page != null) {
            ++MainActivity.this.pageNumber;
            int pageNumber = Integer.parseInt(page.getPageNum());
            int totalItems = Integer.parseInt(page.getTotalContentItems());
            int pageSize = Integer.parseInt(page.getPageSize());
            if (pageNumber == 1) {
                MainActivity.this.totalItems =totalItems;
                totalFetched = pageSize;
                mContentList = new ArrayList();
            } else {
                totalFetched += pageSize;
            }
            mContentList.addAll(page.getContentItems().getContent());
            updateUI(page);
        }

    }


    private class ImageAdapter extends BaseAdapter {

        /*private static final String[] IMAGE_URLS = {};*/

        private LayoutInflater inflater;

        ImageAdapter(Context context) {
            inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return mContentList.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder holder;
            View view = convertView;
            if (view == null) {
                view = inflater.inflate(R.layout.item_grid_image, parent, false);
                holder = new ViewHolder();
                assert view != null;
                holder.imageView = (ImageView) view.findViewById(R.id.image);
                holder.progressBar = (ProgressBar) view.findViewById(R.id.progress);
                holder.description = (TextView) view.findViewById(R.id.description);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            Glide.with(MainActivity.this)
                    .load(jpegToResourceId(mContentList.get(position).getPosterImage()))
                    .override(600, 200)
                    .fitCenter()
                    .into(holder.imageView);
            holder.description.setText(mContentList.get(position).getName());
            return view;
        }
    }

    static class ViewHolder {
        ImageView imageView;
        ProgressBar progressBar;
        TextView description;
    }
    private void readPageAndUpdate(final int rid , final DataFetchCallBack dataFetchCallBack) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Resources res = getResources();
                    InputStream in_s = res.openRawResource(rid);
                    byte[] b = new byte[in_s.available()];
                    in_s.read(b);
                   String str = new String(b);
                    Example exm = gson.fromJson(str , Example.class);
                   dataFetchCallBack.onDataFetched(exm.getPage());
                    //txtHelp.setText(new String(b));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void updateUI(final Page page) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(imageAdapter == null ) {
                    imageAdapter = new ImageAdapter(MainActivity.this);
                    gridView.setAdapter(imageAdapter);
                    gridView.setOnScrollListener(new MyScrollListener() {
                        @Override
                        public boolean onLoadMore(int page, int totalItemsCount) {
                            // Triggered only when new data needs to be appended to the list
                            return checkPullConditions();
                        }
                    });
                    title.setText(page.getTitle());

                }  else {
                    imageAdapter.notifyDataSetChanged();

                }
            }
        });
    }

    private int getResourceId(int pageNumber) {
        switch(pageNumber) {

            case 0: return  R.raw.contentlistingpage_page1;
            case 1: return  R.raw.contentlistingpage_page2;
            case 2: return  R.raw.contentlistingpage_page3;

        }
        return -1;
    }

    private boolean checkPullConditions() {
        if(totalItems >= totalFetched) {
            readPageAndUpdate(getResourceId(pageNumber), MainActivity.this);
            return true;
        }
        return  false;
    }

    private int  jpegToResourceId(String fileName) {

        switch(fileName) {

            case "poster1.jpg" : return R.mipmap.poster1;
            case "poster2.jpg" : return R.mipmap.poster2;

            case "poster3.jpg" : return R.mipmap.poster3;

            case "poster4.jpg" : return R.mipmap.poster4;

            case "poster5.jpg" : return R.mipmap.poster5;
            case "poster6.jpg" : return R.mipmap.poster6;
            case "poster7.jpg" : return R.mipmap.poster7;
            case "poster8.jpg" : return R.mipmap.poster8;
            case "poster9.jpg" : return R.mipmap.poster9;

        }
        return R.mipmap.poster1;

    }


}
